=====================
 Middleware handlers
=====================

The class ``MiddlewareHandler`` is an implementation of middlewares for `Tornado <https://www.tornadoweb.org>`_, where be able a programmatic and ``OOP`` oriented middleware handling and inhertance.


``MiddlewareHandler`` usage and description
-------------------------------------------
Is a base handler where can implement all middlewares.

**Example:**
    .. sourcecode:: python

        # middlewares.py
        from tornado_middleware import MiddlewareHandler
        class EchoMiddleware(MiddlewareHandler):
            # Middleware function must start with 'middleware' or '_middleware'
            async def middleware_echo(self, next):
                print('Echo')
                await next()

        class GreetingsMiddleware(MiddlewareHandler):
            # Middleware function must start with 'middleware' or '_middleware'
            async def middleware_greets(self, next):
                print('Hello ' + self.request.headers.get('User-Agent', 'client'))
                await next()

        # routes.py
        from middlewares import EchoMiddleware, GreetingsMiddleware
        from tornado.web import RequestHandler

        class IndexHandler(EchoMiddleware, GreetingsMiddleware, RequestHandler):
            def get(self):
                self.set_header('Content-Type', 'text/plain; charset=utf-8')
                self.finish('Hello world!!')

``OrderedMiddlewareHandler(MiddlewareHandler)`` usage and description
---------------------------------------------------------------------
With ``OrderedMiddlewareHandler`` can inyect ordering and omit several middlewares setting the variable ``_middleware_order``

**Example:**
    .. sourcecode:: python

        # middlewares.py
        from tornado_middleware import MiddlewareHandler
        class EchoMiddleware(MiddlewareHandler):
            # Middleware function must start with 'middleware' or '_middleware'
            async def middleware_echo(self, next):
                print('Echo')
                await next()

        class GreetingsMiddleware(MiddlewareHandler):
            # Middleware function must start with 'middleware' or '_middleware'
            async def middleware_greets(self, next):
                print('Hello ' + self.request.headers.get('User-Agent', 'client'))
                await next()

        # routes.py
        from middlewares import EchoMiddleware, GreetingsMiddleware
        from tornado_middleware import OrderedMiddlewareHandler
        from tornado.web import RequestHandler

        class IndexHandler(
            EchoMiddleware,
            GreetingsMiddleware,
            OrderedMiddlewareHandler,
            RequestHandler
        ):
            # Trigger first `GreetingsMiddleware.middleware_greets`
            # and then trigger `EchoMiddleware.middleware_echo`
            _middleware_order = ['middleware_greets', 'middleware_echo']
            def get(self):
                self.set_header('Content-Type', 'text/plain; charset=utf-8')
                self.finish('Hello world!!')