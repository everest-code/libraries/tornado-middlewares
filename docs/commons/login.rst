==================
 Login Middleware
==================

``LoginMiddlewareHandler`` is a common wrapper for use a login with method **Basic** authorization, setting a function named ``login``.

**Example:**

    .. sourcecode:: python

        from tornado_middleware.commons import LoginMiddlewareHandler

        class LoginMiddleware(LoginMiddlewareHandler):
            async def login(self, user, password):
                return user == self.user and password == self.password

            def post(self):
                # Do stuff...