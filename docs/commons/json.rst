========================
 JSON Schema Middleware
========================

``JSONSchemaValidationMiddleware`` is a common middleware for set a schema validation.

.. important:: This validator need the dependency ``jsonschema``, can be installed with:

    .. sourcecode:: bash

        pip install jsonschema

**Example:**

    .. sourcecode:: python

        from tornado_middleware.commons import JSONSchemaValidationMiddleware
        from tornado.escape import json_decode

        class AddUserHandler(JSONSchemaValidationMiddleware):
            schema = {
                'type': 'object',
                'required': ['userName', 'password'],
                'properties': {
                    'userName': {'type': 'string'},
                    'password': {'type': 'string', 'minLength': 8},
                },
            }

            def post(self):
                body = json_decode(self.request.body)
                # Do stuff..