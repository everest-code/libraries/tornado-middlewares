===============
 Documentation
===============

``Tornado Middleware`` is a little library, where makes possible create programmatically middlewares, using ``OOP`` and ``Python`` multiple inheritance.

|Contents|

.. toctree::
   :maxdepth: 1

   middleware
   commons/index