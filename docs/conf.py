# -*- coding: utf-8 -*-
# fmt:off

source_suffix = ".rst"
master_doc = "index"

# General information about the project.
project = u"Tornado Middlewares"
copyright = u"2022, Everest Code"
author = u"SGT911"
version = u"v1.2.3"

language = "en"
pygments_style = "sphinx"
html_theme = "sphinx_rtd_theme"

html_theme_options = {
    "collapse_navigation" : False
}

rst_epilog = """
.. include:: substitutions.txt
"""
