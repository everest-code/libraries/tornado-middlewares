## Ignore if not in package development ##
# fmt:off
import sys, pathlib
sys.path.append(str(pathlib.Path(__file__).parent.parent))
# fmt:on
## Ignore if not in package development ##

from tornado_middleware import MiddlewareHandler
from tornado.web import Application


class IndexHandler(MiddlewareHandler):
    async def middleware_echo(self, next):
        print("Middleware triggered")
        await next()

    def get(self):
        print("Function triggered")
        self.set_status(200)
        self.finish("<html><body><h1>Hello Middleware</h1></body></html>")


app = Application([(r"^/$", IndexHandler)], debug=True)

if __name__ == "__main__":
    from tornado.ioloop import IOLoop

    app.listen(8888)
    IOLoop.current().start()
