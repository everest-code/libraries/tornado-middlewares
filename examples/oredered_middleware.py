## Ignore if not in package development ##
# fmt:off
import sys, pathlib
sys.path.append(str(pathlib.Path(__file__).parent.parent))
# fmt:on
## Ignore if not in package development ##

from tornado_middleware import MiddlewareHandler, OrderedMiddlewareHandler
from tornado.web import Application


class IndexHandler(OrderedMiddlewareHandler, MiddlewareHandler):
    _middleware_order = ["middleware1", "middleware3", "middleware2"]

    async def middleware1(self, next):
        print("Triggered middleware #1")
        return await next()

    async def middleware2(self, next):
        print("Triggered middleware #2")
        return await next()

    async def middleware3(self, next):
        print("Triggered middleware #3")
        return await next()

    def get(self):
        print("Function triggered")
        self.set_status(200)
        self.finish("<html><body><h1>Hello Middleware</h1></body></html>")


app = Application([(r"^/$", IndexHandler)], debug=True)

if __name__ == "__main__":
    from tornado.ioloop import IOLoop

    app.listen(8888)
    IOLoop.current().start()
